package upc.edu.pe.inteligencia.artificial;

import org.apache.commons.math3.stat.StatUtils;
import org.apache.commons.math3.stat.regression.SimpleRegression;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import org.math.plot.Plot2DPanel;
import org.math.plot.plotObjects.BaseLabel;

import javax.swing.*;
import java.awt.*;
import java.io.InputStream;

public class Application {

	double[] x = {};
	double[] y = {};

	SimpleRegression sr = new SimpleRegression(true);
	Plot2DPanel plot2D = new Plot2DPanel();
	JTextArea resultado = new JTextArea();
	Dimension ss = Toolkit.getDefaultToolkit().getScreenSize();
	Dimension frameSize = new Dimension ( 600, 600 );

	public static void main(String[] args) {
		new Application();
	}

	public Application(){

		//Importar archivo excel
		InputStream is = getClass().getClassLoader().getResourceAsStream("Estadisticos_Metropolitano.xlsx");

		try {

			//Leyendo archivo excel
			XSSFWorkbook workbook = new XSSFWorkbook(is);
			XSSFSheet worksheet = workbook.getSheetAt(0);

			x = new double[worksheet.getPhysicalNumberOfRows()-1];
			y = new double[worksheet.getPhysicalNumberOfRows()-1];

			//Recorriendo las filas de la hoja de excel
			for(int i=1; i<worksheet.getPhysicalNumberOfRows(); i++) {
				XSSFRow row = worksheet.getRow(i);
				x[i-1] =  row.getCell(1).getNumericCellValue();
				y[i-1] =  row.getCell(2).getNumericCellValue();
			}

			//Llenado de la data en SimpleRegression solo donde los valores de Y son diferente de cero
			for (int i = 0; i < x.length; i++) {
				if(y[i]>0){
					sr.addData(x[i], y[i]);
				}
			}

			//Obtener pronostico en base a la ecuacion de la pendiente obtenida en weka y llenado de data en SimpleRegression
			for (int i = 0; i < x.length; i++) {
				if(y[i]==0){
					y[i] = obtenerPronosticoMes(x[i]) ;
					sr.addData(x[i], y[i]);
				}
			}

			//Obteniendo la linea de la pendiente
			double[] yc = new double[y.length];
			for (int i = 0; i < x.length; i++) {
				yc[i] = sr.predict(x[i]);
			}

			//Llenando toda la informacion obtenida en Plot2DPanel para mostrarlo visualmente
			plot2D.addLegend("SOUTH");
			plot2D.addScatterPlot("Datos", x, y);
			plot2D.addLinePlot("Regresion", x, yc);

			BaseLabel bs = new BaseLabel("Diagrama de dispersion de datos", Color.black, 0.5, 1.1);
			plot2D.addPlotable(bs);

			//Impresion de diferente valores obtenidos de la data
			resultado.setBackground(Color.LIGHT_GRAY);
			resultado.append("Datos leidos: " + sr.getN());
			resultado.append("\nOrdenada al origen: "+ sr.getIntercept());
			resultado.append("\nPendiente: "+ sr.getSlope());
			resultado.append("\nR2: "+ sr.getRSquare());
			resultado.append("\nValor minimo: "+ StatUtils.min(y));
			resultado.append("\nValor maximo: "+ StatUtils.max(y));
			resultado.append("\nValor promedio: "+ StatUtils.mean(y));
			resultado.append("\nVarianza: "+ StatUtils.variance(y));
			resultado.append("\nPromedio geometrico: "+ StatUtils.geometricMean(y));
			resultado.append("\nSuma: "+ StatUtils.sum(y));
			resultado.append("\nProducto: "+ StatUtils.product(y));

			//Creacion de Jframe que contendra Plot2DPanel
			JFrame jf = new JFrame("Regresion lineal");
			jf.setDefaultCloseOperation(jf.EXIT_ON_CLOSE);
			jf.add(plot2D, BorderLayout.CENTER);
			jf.add(resultado, BorderLayout.SOUTH);
			jf.setBounds(ss.width/2 - frameSize.width/2,
					ss.height/2 - frameSize.height/2,
					frameSize.width, frameSize.height);
			jf.setVisible(true);
		} catch (Exception e){
			System.out.println(e);
			System.out.println(e.getMessage());
			System.out.println(e.getStackTrace());
		}
	}

	private double obtenerPronosticoMes(double mes){
		double pronostico = -558242.9367 * mes +
				20825039.257 ;
		return pronostico;
	}


}
